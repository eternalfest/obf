import chai from "chai";

import { asMap, obfuscate, sortMap } from "../lib/obf.js";

describe("obf", () => {
  describe("asMap", () => {
    it("[]", () => {
      const actual: Map<string, string> = asMap([]);
      const expected: Map<string, string> = new Map();
      chai.assert.deepEqual(actual, expected);
    });

    it("[\"foo\"]", () => {
      const actual: Map<string, string> = asMap(["foo"]);
      const expected: Map<string, string> = new Map([["foo", "foo"]]);
      chai.assert.deepEqual(actual, expected);
    });

    it("[\"foo\", \"bar\"]", () => {
      const actual: Map<string, string> = asMap(["foo", "bar"]);
      const expected: Map<string, string> = new Map([["foo", "foo"], ["bar", "bar"]]);
      chai.assert.deepEqual(actual, expected);
    });

    it("{}", () => {
      const actual: Map<string, string> = asMap({});
      const expected: Map<string, string> = new Map();
      chai.assert.deepEqual(actual, expected);
    });

    it("{foo: \"bar\"}", () => {
      const actual: Map<string, string> = asMap({foo: "bar"});
      const expected: Map<string, string> = new Map([["foo", "bar"]]);
      chai.assert.deepEqual(actual, expected);
    });
  });

  describe("sortMap", function () {
    it("new Map([[\"foo\", \"foo\"], [\"bar\", \"bar\"]])", () => {
      const actual: Map<string, string> = sortMap(new Map([["foo", "foo"], ["bar", "bar"]]));
      const expected: Map<string, string> = sortMap(new Map([["bar", "bar"], ["foo", "foo"]]));
      chai.assert.deepEqual([...actual], [...expected]);
    });
  });

  describe("obfuscate", function () {
    it("a", () => {
      const actual: string = obfuscate("xml_adventure", null, new Map());
      const expected: string = "xml_adventure";
      chai.assert.deepEqual(actual, expected);
    });
    it("b", () => {
      const actual: string = obfuscate("xml_adventure", null, new Map([["xml_adventure", "foo"]]));
      const expected: string = "foo";
      chai.assert.deepEqual(actual, expected);
    });
    it("c", () => {
      const actual: string = obfuscate("xml_adventure", "test", new Map());
      const expected: string = "9df880cf98";
      chai.assert.deepEqual(actual, expected);
    });
    it("d", () => {
      const actual: string = obfuscate("xml_adventure", "test", new Map([["xml_adventure", "foo"]]));
      const expected: string = "foo";
      chai.assert.deepEqual(actual, expected);
    });
  });
});
