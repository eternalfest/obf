import chai from "chai";

import { normalize } from "../lib/obf.js";

describe("normalize", function () {
  const ITEMS: [string, string][] = [
    ["foo", "foo"],
    ["cast", "cast"],
    ["_cast", "cast"],
    ["__cast", "_cast"],
    ["Cast", "Cast"],
    ["_Cast", "_Cast"],
    ["__Cast", "__Cast"],
    ["final", "final"],
    ["_final", "final"],
    ["throw", "throw"],
    ["_throw", "throw"],
    ["throwError", "throwError"],
    ["_throwError", "_throwError"],
    ["__throwError", "__throwError"],
    ["doCast", "doCast"],
    ["docast", "docast"],
    ["do_cast", "do_cast"],
    ["_do_cast", "_do_cast"],
    ["_docast", "_docast"],
    ["__do_cast", "__do_cast"],
    ["__docast", "__docast"],
    ["__dollar__", "$"],
    ["___dollar__", "__dollar__"],
    ["____dollar__", "___dollar__"],
    ["__dollar__map", "$map"],
    ["___dollar__map", "__dollar__map"],
    ["____dollar__map", "___dollar__map"],
    ["map__dollar__", "map$"],
    ["map___dollar__", "map__dollar__"],
    ["__dollar__dollar__", "$dollar__"],
    ["__dollar___dollar__", "$_dollar__"],
    ["__dollar____dollar__", "$$"],
    ["___dollar____dollar__", "__dollar__$"],
    ["___dollar___dollar__", "__dollar___dollar__"],
    ["___dollar__dollar__", "__dollar__dollar__"],
    ["___dollar_____dollar__", "__dollar____dollar__"],
  ];

  for (const [input, expected] of ITEMS) {
    it(`normalize(${JSON.stringify(input)})`, function () {
      const actual: string = normalize(input);
      chai.assert.strictEqual(actual, expected);
    });
  }
});
