import fs from "fs";
import url from "url";

export async function readTextFile(filePath: url.URL): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(filePath, {encoding: "utf8"}, (
      err: NodeJS.ErrnoException | null,
      text: string,
    ): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(text);
      }
    });
  });
}
