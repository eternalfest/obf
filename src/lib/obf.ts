import { createHash } from "crypto";
import { fromSysPath, Furi, join as furiJoin } from "furi";
import { emitSwf } from "swf-emitter";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Movie, TagType } from "swf-types";
import { Metadata } from "swf-types/lib/tags";

import { readTextFile } from "./fs-utils.js";
import { mapMovie } from "./map-strings.js";
import meta from "./meta.js";

// export async function obfuscate(
//   inputFile: url.URL,
//   outputFile: url.URL,
//   mapFile: url.URL,
//   key: string,
// ): Promise<void> {
//   const inBytes: Uint8Array = await readFile(inputFile);
//   const result: ObfuscationResult = Obfuscator.obfuscate(inBytes, key);
//   const mapRecord: Record<string, string> = Object.create(null);
//   for (const [newStr, oldStr] of result.map) {
//     Reflect.set(mapRecord, newStr, oldStr);
//   }
//   await Promise.all([
//     outputFileAsync(outputFile, result.bytes),
//     outputFileAsync(mapFile, Buffer.from(`${JSON.stringify(mapRecord, null, 2)}\n`)),
//   ]);
// }

export type MapLike =
  ReadonlyMap<string, string>
  | Record<string, string>
  | readonly string[]
  | ReadonlySet<string>;

export function asMap(mapLike: MapLike): Map<string, string> {
  if (mapLike instanceof Set) {
    const result: Map<string, string> = new Map();
    for (const id of mapLike) {
      result.set(id, id);
    }
    return result;
  } else if (Array.isArray(mapLike)) {
    const result: Map<string, string> = new Map();
    for (const id of mapLike) {
      if (result.has(id)) {
        throw new Error(`DuplicateId: ${id}`);
      }
      result.set(id, id);
    }
    return result;
  } else if (mapLike instanceof Map) {
    return new Map([...mapLike]);
  } else {
    return new Map(Object.entries(mapLike));
  }
}

export function mergeMaps(base: MapLike, extra: MapLike): Map<string, string> {
  const result: Map<string, string> = asMap(base);
  for (const [clear, obf] of asMap(extra)) {
    const baseObf: string | undefined = result.get(clear);
    if (baseObf !== undefined && baseObf !== obf) {
      throw new Error(`ObfMapConflict: ${JSON.stringify(clear)} -> ${JSON.stringify([baseObf, obf])}`);
    }
    result.set(clear, obf);
  }
  return result;
}

export function getAs2MapUri(): Furi {
  return furiJoin(fromSysPath(meta.dirname), "as2.map.json");
}

export async function getAs2Map(): Promise<Map<string, string>> {
  const furi: Furi = getAs2MapUri();
  const jsonStr: string = await readTextFile(furi);
  const raw: string[] = JSON.parse(jsonStr);
  return asMap(raw);
}

export function sortMap<T>(map: ReadonlyMap<string, T>): Map<string, T> {
  const entries: [string, T][] = [...map];
  entries.sort((left: [string, T], right: [string, T]): -1 | 0 | 1 => {
    if (left[0] === right[0]) {
      return 0;
    } else {
      return left[0] < right[0] ? -1 : 1;
    }
  });
  return new Map(entries);
}

export function obfuscateBytes(
  movie: Uint8Array,
  key: string | null,
  parentMap: ReadonlyMap<string, string>,
): ObfuscationResult {
  const parsed: Movie = parseSwf(movie);
  const map: Map<string, string> = obfuscateMovie(parsed, key, parentMap);
  const bytes: Uint8Array = emitSwf(parsed, CompressionMethod.Deflate);
  return {bytes, map};
}

export function obfuscateMovie(
  movie: Movie,
  key: string | null,
  parentMap: ReadonlyMap<string, string>,
): Map<string, string> {
  if (isObfuscated(movie)) {
    throw new Error("AlreadyObfuscated");
  }
  const result: Map<string, string> = Obfuscator.obfuscate(movie, key, parentMap);
  flagAsObfuscated(movie);
  return result;
}

export function isObfuscated(movie: Movie): boolean {
  for (const tag of movie.tags) {
    if (tag.type === TagType.Metadata) {
      let meta: any;
      try {
        meta = tag.metadata;
      } catch (e) {
        continue;
      }
      if (meta === null || !(meta instanceof Object)) {
        continue;
      }
      if (meta.obf === true) {
        return true;
      }
    }
  }
  return false;
}

function flagAsObfuscated(movie: Movie): void {
  const tag: Metadata = {
    type: TagType.Metadata,
    metadata: JSON.stringify({obf: true}),
  };
  movie.tags.unshift(tag);
}

export interface ObfuscationResult {
  bytes: Uint8Array;
  map: Map<string, string>;
}

export function obfuscate(old: string, key: string | null, parentMap: ReadonlyMap<string, string>): string {
  const salt: string | null = key !== null
    ? createHash("md5").update(Buffer.from(key)).digest("hex")
    : null;
  old = normalize(old);
  const dbStr: string | undefined = parentMap.get(old);
  if (dbStr !== undefined) {
    return dbStr;
  }
  if (/^@+[^@]/.test(old)) {
    return old.substring(1);
  }
  if (!isIdentifier(old) || isProtected(old)) {
    return old;
  }
  return hash(old, salt);
}

function hash(clear: string, salt: string | null): string {
  if (salt === null) {
    return clear;
  }
  return createHash("md5")
    .update(Buffer.from(`${salt}${clear}`))
    .digest("hex")
    .substring(0, 10);
}

class Obfuscator {
  private _map: Map<string, string>;
  private salt: string | null;

  private constructor(salt: string | null, parentMap: ReadonlyMap<string, string>) {
    this._map = new Map([...parentMap]);
    this.salt = salt;
  }

  public static obfuscate(
    movie: Movie,
    key: string | null,
    parentMap: ReadonlyMap<string, string>,
  ): Map<string, string> {
    const salt: string | null = key !== null
      ? createHash("md5").update(Buffer.from(key)).digest("hex")
      : null;
    const obf: Obfuscator = new Obfuscator(salt, parentMap);
    obf.run(movie);
    return sortMap(obf._map);
  }

  private run(movie: Movie): void {
    mapMovie((s: string) => this.map(s), movie);
  }

  private map(old: string): string {
    old = normalize(old);
    const dbStr: string | undefined = this._map.get(old);
    if (dbStr !== undefined) {
      return dbStr;
    }
    if (/^@+[^@]/.test(old)) {
      return old.substring(1);
    }
    if (!isIdentifier(old) || isProtected(old)) {
      return old;
    }
    const newStr: string = hash(old, this.salt);
    this._map.set(old, newStr);
    return newStr;
  }
}

/**
 * Preprocess identifier as a workaround of the rules used by Haxe.
 *
 * If an identifier starts with one or many `_` followed by `final`, `throw` or
 * `cast`, it removes the first `_`.
 * If an identifier contains `__dollar__`, it is replaced with `$`. You can
 * escape it with `_`.
 */
export function normalize(id: string): string {
  if (/^_+(?:cast|final|throw)$/.test(id)) {
    return id.substring(1);
  } else if (/_{2,}dollar__/.test(id)) {
    return id.replace(
      /_{2,}dollar__/g,
      (str: string): string => str === "__dollar__" ? "$" : str.substring(1),
    );
  } else {
    return id;
  }
}

function isIdentifier(str: string): boolean {
  return /^[A-Za-z_$][A-Za-z0-9_$]*$/.test(str);
}

function isProtected(str: string): boolean {
  return str.startsWith("$");
}
