import { cfgToBytes } from "avm1-emitter";
import { parseCfg } from "avm1-parser";
import { ActionType } from "avm1-types/lib/action-type.js";
import { CatchTargetType } from "avm1-types/lib/catch-targets/_type.js";
import { Action as CfgAction } from "avm1-types/lib/cfg/action.js";
import {
  ConstantPool,
  DefineFunction as CfgDefineFunction,
  DefineFunction2 as CfgDefineFunction2,
  GotoLabel,
  Push,
  SetTarget,
} from "avm1-types/lib/cfg/actions/index.js";
import { Cfg } from "avm1-types/lib/cfg/cfg.js";
import { CfgBlock } from "avm1-types/lib/cfg/cfg-block.js";
import { CfgFlow } from "avm1-types/lib/cfg/cfg-flow.js";
import { CfgFlowType } from "avm1-types/lib/cfg/cfg-flow-type.js";
import { PushValueType } from "avm1-types/lib/push-value-type.js";
import { emitSwf } from "swf-emitter";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Movie, Tag, TagType } from "swf-types";
import {
  DefineDynamicText,
  DefineSceneAndFrameLabelData,
  DefineSprite,
  DoAction,
  DoInitAction,
  ExportAssets,
  FrameLabel,
  PlaceObject
} from "swf-types/lib/tags";

export type StringMapper = (source: string) => string;

export function extractStrings(source: Uint8Array): Set<string> {
  const strings: Set<string> = new Set();
  const movie: Movie = parseSwf(source);
  mapMovie(
    (x: string): string => {
      strings.add(x);
      return x;
    },
    movie,
  );
  return strings;
}

export function mapMovieBytes(fn: StringMapper, source: Uint8Array): Uint8Array {
  const movie: Movie = parseSwf(source);
  mapMovie(fn, movie);
  return emitSwf(movie, CompressionMethod.Deflate);
}

export function mapMovie(fn: StringMapper, movie: Movie): void {
  for (const tag of movie.tags) {
    mapTag(fn, tag);
  }
}

function mapTag(fn: StringMapper, tag: Tag): void {
  switch (tag.type) {
    case TagType.DefineDynamicText: {
      mapDefineDynamicTextTag(fn, tag);
      break;
    }
    case TagType.DefineSceneAndFrameLabelData: {
      mapDefineSceneAndFrameLabelDataTag(fn, tag);
      break;
    }
    case TagType.DefineSprite: {
      mapDefineSpriteTag(fn, tag);
      break;
    }
    case TagType.DoAction: {
      mapDoActionTag(fn, tag);
      break;
    }
    case TagType.DoInitAction: {
      mapDoInitActionTag(fn, tag);
      break;
    }
    case TagType.ExportAssets: {
      mapExportAssetsTag(fn, tag);
      break;
    }
    case TagType.FrameLabel: {
      mapFrameLabelTag(fn, tag);
      break;
    }
    case TagType.PlaceObject: {
      mapPlaceObjectTag(fn, tag);
      break;
    }
    default:
      break;
  }
}

function mapDefineDynamicTextTag(fn: StringMapper, tag: DefineDynamicText): void {
  if (tag.variableName !== undefined) {
    const segments: string[] = tag.variableName.split("/");
    const mappedSegments: string[] = [];
    for (const [i, segment] of segments.entries()) {
      const isLast: boolean = i === segments.length - 1;
      if (!isLast) {
        mappedSegments.push(fn(segment));
      } else {
        mappedSegments.push(segment.split(".").map(p => fn(p)).join("."));
      }
    }
    (tag as any).variableName = mappedSegments.join("/");
  }
}

function mapDefineSceneAndFrameLabelDataTag(
  fn: StringMapper,
  tag: DefineSceneAndFrameLabelData,
): void {
  for (const scene of tag.scenes) {
    scene.name = fn(scene.name);
  }
  for (const label of tag.labels) {
    label.name = fn(label.name);
  }
}

function mapDefineSpriteTag(fn: StringMapper, tag: DefineSprite): void {
  for (const spriteTag of tag.tags) {
    mapTag(fn, spriteTag);
  }
}

function mapDoActionTag(fn: StringMapper, tag: DoAction): void {
  const cfg: Cfg = parseCfg(tag.actions);
  mapCfg(fn, cfg);
  (tag as any).actions = cfgToBytes(cfg as any);
}

function mapDoInitActionTag(fn: StringMapper, tag: DoInitAction): void {
  const cfg: Cfg = parseCfg(tag.actions);
  mapCfg(fn, cfg);
  (tag as any).actions = cfgToBytes(cfg as any);
}

function mapExportAssetsTag(fn: StringMapper, tag: ExportAssets): void {
  for (const asset of tag.assets) {
    asset.name = fn(asset.name);
  }
}

function mapFrameLabelTag(fn: StringMapper, tag: FrameLabel): void {
  (tag as any).name = fn(tag.name);
}

function mapPlaceObjectTag(fn: StringMapper, tag: PlaceObject): void {
  if (tag.className !== undefined) {
    (tag as any).className = fn(tag.className);
  }
  if (tag.name !== undefined) {
    (tag as any).name = fn(tag.name);
  }
}

function mapCfg(fn: StringMapper, cfg: Cfg): void {
  for (const block of cfg.blocks) {
    mapCfgBlock(fn, block);
  }
}

function mapCfgBlock(fn: StringMapper, block: CfgBlock): void {
  for (const action of block.actions) {
    mapCfgAction(fn, action);
  }
  const flow: CfgFlow = block.flow;
  switch (flow.type) {
    case CfgFlowType.Try: {
      mapCfg(fn, flow.try);
      if (flow.catch !== undefined) {
        switch (flow.catch.target.type) {
          case CatchTargetType.Variable: {
            flow.catch.target.target = fn(flow.catch.target.target);
            break;
          }
          default:
            break;
        }
        mapCfg(fn, flow.catch.body);
      }
      if (flow.finally !== undefined) {
        mapCfg(fn, flow.finally);
      }
      break;
    }
    case CfgFlowType.With: {
      mapCfg(fn, flow.body);
      break;
    }
    default:
      break;
  }
}

function mapCfgAction(fn: StringMapper, action: CfgAction): void {
  switch (action.action) {
    case ActionType.ConstantPool: {
      mapConstantPoolAction(fn, action);
      break;
    }
    case ActionType.DefineFunction: {
      mapDefineFunctionAction(fn, action);
      break;
    }
    case ActionType.DefineFunction2: {
      mapDefineFunction2Action(fn, action);
      break;
    }
    case ActionType.GotoLabel: {
      mapGotoLabelAction(fn, action);
      break;
    }
    case ActionType.Push: {
      mapPushAction(fn, action);
      break;
    }
    case ActionType.SetTarget: {
      mapSetTargetAction(fn, action);
      break;
    }
    default:
      break;
  }
}

function mapConstantPoolAction(fn: StringMapper, action: ConstantPool): void {
  action.pool = action.pool.map(fn);
}

function mapDefineFunctionAction(fn: StringMapper, action: CfgDefineFunction): void {
  action.name = fn(action.name);
  action.parameters = action.parameters.map(fn);
  mapCfg(fn, action.body);
}

function mapDefineFunction2Action(fn: StringMapper, action: CfgDefineFunction2): void {
  action.name = fn(action.name);
  for (const parameter of action.parameters) {
    parameter.name = fn(parameter.name);
  }
  mapCfg(fn, action.body);
}

function mapGotoLabelAction(fn: StringMapper, action: GotoLabel): void {
  action.label = fn(action.label);
}

function mapPushAction(fn: StringMapper, action: Push): void {
  for (const value of action.values) {
    switch (value.type) {
      case PushValueType.String:
        value.value = fn(value.value);
        break;
      default:
        break;
    }
  }
}

function mapSetTargetAction(fn: StringMapper, action: SetTarget): void {
  action.targetName = fn(action.targetName);
}
