# 0.2.0 (2020-02-24)

- **[Breaking change]** Update to `swf-types@0.11` and `avm1-types@0.10`.

# 0.1.4 (2020-01-27)

- **[Feature]** Add `obfuscate` function to obfuscate a single string.
- **[Fix]** Update dependencies.

# 0.1.3 (2019-12-28)

- **[Fix]** Export `ObfuscationResult`.
- **[Internal]** Update dev-dependencies.

# 0.1.2 (2019-12-18)

- **[Fix]** Blacklist conflicting identifiers from `as2.map.json`.
- **[Fix]** Sort obfuscation map.

# 0.1.1 (2019-12-18)

- **[Fix]** Fix `asMap` support for records.

# 0.1.0 (2019-12-18)

- **[Feature]** First release.
