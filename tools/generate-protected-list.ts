import cheerio from "cheerio";
import fs from "fs";
import path from "path";
import process from "process";

function scrapeIdentifiers(file: string): string[] {
  const content = fs.readFileSync(file, { encoding: "utf-8" });
  const $ = cheerio.load(content);

  const idents: string[] = [];
  $("td.idxrow").each((_, elem) => {
    const text = $(elem).text();
    const match = text.match(/([_A-Za-z][_A-Za-z0-9.]*).* — ([^ ]+).*/);
    if (match === null) {
      throw new Error(`Couldn't match text: '${text}' in file ${file}`);
    }

    const ident = match[1];
    const kind = match[2];

    switch (kind) {
      case "Statement":
      case "Operator":
      case "Compiler":
        break; // ignore

      case "Class":
      case "Constant":
      case "Constructor":
      case "Event":
      case "Global":
      case "Method":
      case "Property":
      case "Static":
      case "Dynamic":
        idents.push(ident);
        break;

      case "Package":
        for (const part of ident.split(".")) {
          idents.push(part);
        }
        break;

      default:
        throw new Error(`Unknown identifier kind: '${kind}' in file ${file}`);
    }
  });

  return idents;
}

const BLACKLIST: ReadonlySet<string> = new Set([
  "a",
  "alpha",
  "alphas",
  "b",
  "c",
  "colors",
  "d",
  "decode",
  "lang",
  "menu",
  "onCancel",
  "prefix",
  "thickness",
  "time",
  "tx",
  "ty",
  "version",
  "x",
  "y",
]);

function main() {
  const args: readonly string[] = process.argv.slice(2);
  if (args.length !== 1) {
    console.log("Usage: ts-node generate-proctected-list.ts <path-to-flash-ref>");
    process.exit(1);
  }

  const [referencePath] = args;
  const identifiers: Set<string> = new Set();

  for (const letter of "ABCDEFGHIJKLMNOPQRSTUVWXYZ") {
    for (const ident of scrapeIdentifiers(path.join(referencePath, `all-index-${letter}.html`))) {
      if (!BLACKLIST.has(ident)) {
        identifiers.add(ident);
      }
    }
  }

  const identifiersList: string[] = [...identifiers];
  identifiersList.sort();

  process.stdout.write(JSON.stringify(identifiersList, undefined, 2));
  process.stdout.write("\n");
}

main();
